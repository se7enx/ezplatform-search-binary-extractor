<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformSearchBinaryExtractorBundle;

use ContextualCode\EzPlatformSearchBinaryExtractorBundle\DependencyInjection\EzPlatformSearchBinaryExtractorExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class EzPlatformSearchBinaryExtractorBundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        return new EzPlatformSearchBinaryExtractorExtension();
    }
}
