<?php

namespace ContextualCode\EzPlatformSearchBinaryExtractorBundle\FieldType\BinaryFile;

use eZ\Publish\SPI\Persistence\Content\Field;

interface BinaryExtractor
{
    public function supports(Field $field): bool;

    public function extract(Field $field): ?string;
}
