<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformSearchBinaryExtractorBundle\FieldType\BinaryFile\BinaryExtractor;

use ContextualCode\EzPlatformSearchBinaryExtractorBundle\FieldType\BinaryFile\BinaryExtractor;
use eZ\Publish\Core\IO\IOServiceInterface;
use eZ\Publish\Core\IO\Values\BinaryFile;
use eZ\Publish\SPI\Persistence\Content\Field;
use Symfony\Component\Process\Process;

class Pdf implements BinaryExtractor
{
    protected const SUPPORTED_MIMETYPES = ['application/pdf'];

    /** @var IOServiceInterface */
    private $ioService;

    public function __construct(IOServiceInterface $ioService)
    {
        $this->ioService = $ioService;
    }

    public function supports(Field $field): bool
    {
        return
            isset($field->value->externalData) &&
            isset($field->value->externalData['mimeType']) &&
            in_array($field->value->externalData['mimeType'], self::SUPPORTED_MIMETYPES, true);
    }

    public function extract(Field $field): ?string
    {
        $file = $this->ioService->loadBinaryFile($field->value->externalData['id']);
        if ($file instanceof BinaryFile === false) {
            return null;
        }

        if (!$this->ioService->exists($file->id)) {
            return null;
        }

        // DFS/AWS binary data handler might be in use, so we need to file content into the local file
        $tmpFile = tmpfile();
        fwrite($tmpFile, $this->ioService->getFileContents($file));
        $tmpMetadata = stream_get_meta_data($tmpFile);
        $text = $this->pdfToText($tmpMetadata['uri']);
        fclose($tmpFile);

        return $text;
    }

    protected function pdfToText(string $filepath): ?string
    {
        $process = new Process(['bin/pdftotext', '-nopgbrk', '-q', $filepath, '-']);
        $process->run();

        $text = trim($process->getOutput());

        return $this->filterString($text);
    }

    protected function filterString(string $string): string
    {
        $string = mb_convert_encoding($string, 'UTF-8', 'UTF-8');

        return preg_replace('/[^[:print:]\n]/u', '', $string);
    }
}
